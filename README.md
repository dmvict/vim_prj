# vim_prj

### Plugin for managing projects

Fork of [Bashka/vim_prj](https://github.com/Bashka/vim_prj) on GitHub.

### About

The author of the first version of the plugin created a plugin system and a base library for it. A time ago I've used it without any changes. But some features was disgusted for me. So, this fork shows my vision of user projects and sessions managing.

#### Features

- **Autoloading and autosaving** - Plugin automatically saves and restores projects. Also, the plugin can save and load the last user session.
- **Graphical user interface** - All actions with projects can be performed by calling single function. Type next command to open the interface `call vim_prj#showProjectsList()` and follow help in an opened buffer.
- **Fast switching between projects** - user can leave current project and switch to another, the working session will be saved.
- **Sorting of projects** - Projects can be sorted by name, date of last saving, by order id.
- **Reordering of projects** - The position of any project can be changed.
- **Renaming** - User can rename project.
- **Local and remote projects directories** - Each project has its directory. By default, the plugin saves project in a local directory (current working directory), but user can use a special directory for collecting of all projects.

### Installation

Plugin **vim_prj** depends on one base library [**vim_lib**](https://gitlab.com/dmvict/vim_lib).

If you are using some plugin manager install both plugins.

#### Installation with **Pathnogen**

To install plugin run next commands:

```
git clone https://gitlab.com/dmvict/vim_lib ~/.vim/bundle/
git clone https://gitlab.com/dmvict/vim_prj ~/.vim/bundle/
```

#### Installation using **vim_lib** plugin

The base library **vim_lib** inside have a plugin manager. It works like **Vundle**.

The manual of using plugin manager in library is given in the documentation. It's a simplified example.

Add next code to start of your `.vimrc` file:

```
filetype off
set rtp=~/.vim/bundle/vim_lib
call vim_lib#sys#Autoload#init('~/.vim', 'bundle')
Plugin 'vim_lib'
Plugin 'vim_prj'

" other plugins

filetype indent plugin on
```

The variable `rtp` defines path to library. It initialize command `Plugin`. So, line `Plugin [name]` connects plugin with specified name in directory that defined in routine `init` - in example uses path `~/.vim' + 'bundle => ~/.vim/bundle/`, but plugin can use any path.


### P.S.

I'm not a Vim plugins developer. It's work for fun and improving daily usage of my favorite editor. If you have the best solution for some routines you can create PR or send me a message.
Thanks!
