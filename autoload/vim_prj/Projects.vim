" Author: Dmytro K. (dm.vict.kr@gmail.com)
" License: GNU GPL v3 (http://www.gnu.org/copyleft/gpl.html)

let s:Path = vim_lib#base#Path#
let s:File = vim_lib#base#File#
let s:System = vim_lib#sys#System#.new()
let s:BufferStack = vim_lib#view#BufferStack#
let s:Buffer = vim_lib#sys#Buffer#
let s:Content = vim_lib#sys#Content#.new()

"//

let s:screen = s:Buffer.new( '#Projects#' )
call s:screen.temp()

call s:screen.option( 'wrap', 0 )
call s:screen.option( 'number', 0 )
call s:screen.option( 'cursorline', 1 )
call s:screen.option( 'winfixwidth', 1 )
call s:screen.option( 'spell', 0 )
call s:screen.option( 'filetype', 'vim-prj' )
call s:screen.au( 'InsertEnter',  'autoLeaveInsertMode' )
call s:screen.au( 'CursorMovedI', 'autoLeaveInsertMode' )
call s:screen.au( 'CursorMoved',  'updateProjectsListByTime' )
call s:screen.au( 'CursorHold',   'updateProjectsListByTime' )
call s:screen.au( 'WinEnter',     'updateProjectsListByTime' )

"//

""
" @param { String } sortValue - Sets default value for sorting projects.
""

let s:screen.sortValue = 'orderId'

"//

""
" @param { BoolLike } reverseSort - Sets direction of sorting.
""

let s:screen.reverseSort = 0

"//

""
" @param { BoolLike } changes - The flag that shows changes in project list.
""

let s:screen.changes = 0

"//

call s:screen.map( 'n', '<Enter>',          'selectProject' )
call s:screen.map( 'n', '<2-LeftMouse>',    'selectProject' )
call s:screen.map( 'n', 'aa',               'addProject' )
call s:screen.map( 'n', '<Leader>aa',       'createProject' )
call s:screen.map( 'n', '<Leader>dd',       'deleteProject' )
call s:screen.map( 'n', 'd',                'deleteRecord' )
call s:screen.map( 'n', 'i',                'showProjectInfo' )
call s:screen.map( 'n', 'e',                'editVimrc' )
call s:screen.map( 'n', 'cn',               'renameProject' )
call s:screen.map( 'n', '<C-Up>',           'recordUp' )
call s:screen.map( 'n', '<C-Down>',         'recordDown' )
call s:screen.map( 'n', '<S-r>',            'reload' )
call s:screen.map( 'n', 's',                'setSorter' )
call s:screen.map( 'n', 'r',                'reverseProjectList' )
call s:screen.map( 'n', 'q',                'closeWindow' )
call s:screen.map( 'n', '?',                'showHelp' )

"//

""
" Method autoLeaveInsertMode() sends escape sequence in editor. If it use with
" some autocommand editor will leave insert mode for some action.
""

function! s:screen.autoLeaveInsertMode()
    call feedkeys( "\<Esc>", "!" )
endfunction

"//

""
" Method equilizer() compares values of field of projects that is declared by parameter {-sortValue-}.
"
" @param { * } e1 - First element to compare.
" @param { * } e2 - Second element to compare.
"
" @returns { Number } - Returns result of comparing:
" -1 - the first element is less than the second;
" 0 - the first and the second element are equal;
" 1 - the first element is greater than the second.
""

function! s:screen.equilizer( e1, e2 )
    if self.reverseSort == 0
        return a:e1[ s:screen.sortValue ] == a:e2[ s:screen.sortValue ] ? 0 : a:e1[ s:screen.sortValue ] > a:e2[ s:screen.sortValue ] ? 1 : -1
    else
        return a:e1[ s:screen.sortValue ] == a:e2[ s:screen.sortValue ] ? 0 : a:e2[ s:screen.sortValue ] > a:e1[ s:screen.sortValue ] ? 1 : -1
    endif
endfunction

""
" Method equilizerForId() compares values of field `orderId` in project records.
"
" @param { Number } e1 - First element to compare.
" @param { Number } e2 - Second element to compare.
"
" @returns { Number } - Returns result of comparing:
" negative number - the first element is less than the second;
" 0 - the first and the second element are equal;
" positive number - the first element is greater than the second.
""

function! s:screen.equilizerForId( e1, e2 )
    if self.reverseSort == 0
        return a:e1.orderId - a:e2.orderId
    else
        return a:e2.orderId - a:e1.orderId
    endif
endfunction

"//

""
" Method sort() sorts provided List {-list-} using equalizer {-equalizer-}.
"
" @param { List } list - The list to sort.
"
" @returns { List } - Returns sorted list.
""

function! s:screen.sort( list )
    if self.sortValue == 'orderId'
        return sort( a:list, self.equilizerForId )
    else
        return sort( a:list, self.equilizer )
    endif
endfunction

"//

""
" Method reverseProjectList() changes sorting direction and redraws projects list.
"
" @returns { List } - Returns not a value, change order of projects.
""

function! s:screen.reverseProjectList()
    let self.reverseSort = self.reverseSort == 1 ? 0 : 1
    call self.sort( self.projects )
    let self.changes = 1
    call self.redraw()
endfunction

"//

""
" Method setSorter() sets the field of project record that compares in {-equilizer-}.
"
" @returns {} - Returns not a value, sets type of sorting.
""

function! s:screen.setSorter()
    call s:System.echo( 'Filters:', 'ModeMsg' )
    call s:System.echo( '0. ID' )
    call s:System.echo( '1. Name' )
    call s:System.echo( '2. Time of closing' )
    call s:System.echo( '3. Time of creation' )
    let l:type = s:System.read( 'Select filter type: ' )
    if l:type != ''
        if s:screen.sortValue == 'timeOfModification'
            let s:screen.reverseSort = s:screen.reverseSort == 1 ? 0 : 1
          endif
        if l:type == 0
            let s:screen.sortValue = 'orderId'
        elseif l:type == 1
            let s:screen.sortValue = 'name'
        elseif l:type == 2
            let s:screen.sortValue = 'timeOfModification'
            let s:screen.reverseSort = s:screen.reverseSort == 1 ? 0 : 1
        elseif l:type == 3
            let s:screen.sortValue = 'timeOfCreation'
        endif
    endif
    call self.sort( self.projects )
    let self.changes = 1
    call self.redraw()
endfunction

"//

""
" Method parse() parses data in file with projects data.
"
" @param { String } file - Absolute path to projects file.
"
" @returns { List } - Returns of projects.
""

function! s:screen.parse( file )
    let l:file = s:File.absolute( a:file ).read()
    let l:projects = []

    let l:length = len( l:file )

    let l:i = 0
    while l:i < l:length
        let l:str = l:file[ l:i ]
        if l:str == ''
            let l:i += 1
            continue
        endif

        if l:str[ 0 ] != ' '
            let l:splited = split( l:str, ':' )
            if len( l:splited ) > 1
                let l:value = s:Content.trim( l:splited[ 1 ] )
                if l:value == '0' || l:value == '1'
                    let self.reverseSort = l:value
                else
                    let self.sortValue = l:value
                endif
            else
                call add( l:projects, { 'name' : s:Content.trim( l:splited[ 0 ] ) } )
            endif
        endif

        if l:str[ 0 ] == ' ' && l:str != ' '
            let l:splited = split( l:str, ':' )
            let l:projects[ len( l:projects ) - 1 ][ s:Content.trim( l:splited[ 0 ] ) ] = s:Content.trim( join( l:splited[ 1 : ], ':' ) )
        endif

        let l:i +=1
      endwhile

    return l:projects
endfunction

""
" @param { List } projects - Local container for list of projects in current session.
""

let s:screen.projects = []
if s:File.absolute( g:vim_prj#.prjList ).isExists()
    let s:screen.projects = s:screen.parse( g:vim_prj#.prjList )
    call s:screen.sort( s:screen.projects )
endif

""
" @param { Number } prjListModificationTime - Get the last modification time" of file at the {-prjList-} path.
""

let s:screen.prjListModificationTime = getftime( g:vim_prj#.prjList )

"//

""
" Method render() makes list of projects for buffer.
"
" @returns { List } - Makes list of projects, returns content of buffer.
""

function! s:screen.render()
    let l:result = [ "   " . '" Projects "', "" ]

    if len( s:screen.projects ) > 0
        let l:length = len( len( s:screen.projects ) )
        let l:i = 0
        for l:project in s:screen.projects
            if type( g:vim_prj#.currentProject ) == 4 && l:project == g:vim_prj#.currentProject
                call add( l:result, s:Content.strDup( ' ', 1 + l:length - len( l:i + 1 ) ) . ( l:i + 1 ) . ']' . "  " . l:project.name . ' *' )
            else
                call add( l:result, s:Content.strDup( ' ', 1 + l:length - len( l:i + 1 ) ) . ( l:i + 1 ) . ']' . "  " . l:project.name )
            endif
            let l:i += 1
        endfor
    else
        let l:info =
        \[
        \    '" aa - add new project',
        \    '" <Leader>aa - create project from current session',
        \]
        let l:result = l:result + l:info
    endif

    call add( l:result, "\n" . 'Press ? for help' . "\n" . 'Press q to quit' )

    return result
endfunction

"//

""
" Method getModificationTime() automatically updates list of projects in buffer.
" The triggers is CursorMoved, CursorHold, WinEnter.
"
" @returns {} - Updates list of projects in buffer, returns not a value.
""

function! s:screen.updateProjectsListByTime()
    let l:currentModificationTime = getftime( g:vim_prj#.prjList )
    if l:currentModificationTime > s:screen.prjListModificationTime
        let s:screen.prjListModificationTime = l:currentModificationTime
        call self.reload()
    endif
endfunction

""
" Method reload() updates list of projects in buffer.
"
" @returns {} - Updates list of projects in buffer, returns not a value.
""

function! s:screen.reload()
    let s:screen.projects = s:screen.parse( g:vim_prj#.prjList )
    call s:screen.sort( s:screen.projects )
    let l:pwd = getcwd()
    for project in self.projects
        if project.pwdPath == l:pwd
            let g:vim_prj#.currentProject = project
        endif
    endfor
    call self.redraw()
endfunction

"//

""
" Method getProjectNumber() returns number in line under cursor.
"
" @returns { Number } - Returns number in line under cursor.
""

function s:screen.getProjectNumber()
    return matchstr( getline( '.' ), '\<\d*' )
endfunction

"//

""
" Method getProject() returns map with project data under cursor.
"
" @returns { Dictionary } - Returns project under cursor.
""

function s:screen.getProject()
    return self.projects[ self.getProjectNumber() - 1 ]
endfunction

"//

""
" Method selectProject() provides switching between projects. To load a
" project select line with project name and press 'Enter'.
"
" @returns {} - Loads projects, opens session with project.
""

function! s:screen.selectProject()
    exe 'silent wall'
    call g:vim_prj#saveSession()
    let self.changes = 1
    if type( g:vim_prj#.currentProject ) != 4
        call s:System.au( 'VimLeavePre', function( "vim_prj#saveSession" ) )
    else
        let g:vim_prj#.currentProject.timeOfModification = exists( "*strftime" ) ? strftime( "%Y%m%d%H%M%S" ) : localtime()
    endif
    let g:vim_prj#.currentProject = self.getProject()
    let g:vim_prj#.currentProject.timeOfModification = ( exists( "*strftime" ) ? strftime( "%Y%m%d%H%M%S" ) : localtime() ) + 1
    call s:screen.sort( s:screen.projects )
    exe 'silent %bd'
    exe 'silent source ' . s:Path.join( g:vim_prj#.currentProject.projectPath, 'session.vim' )
    if s:File.absolute( s:Path.join( g:vim_prj#.currentProject.projectPath, '.vimrc' ) ).isExists() && len( s:File.absolute( s:Path.join( g:vim_prj#.currentProject.projectPath, '.vimrc' ) ).read() ) != 0
        exe 'silent! so ' . s:Path.join( g:vim_prj#.currentProject.projectPath, '.vimrc' )
    endif
endfunction

"//

""
" Method addProject() creates new empty project with current working directory
" in the specified path. The local files of the project can saves at local
" '.vim' directory or in the sessions directory 'g:vim_prj#prjPath'.
"
" @returns {} - Creates new empty projects, returns not a value.
""

function! s:screen.addProject()
    call g:vim_prj#addNewPrj()
    let self.changes = 1
    call self.redraw()
endfunction

"//

""
" Method createProject() creates new project from current working session.
" The local files of the project can saves at local '.vim' directory or in
" the sessions directory 'g:vim_prj#prjPath'.
"
" @returns {} - Creates new projects with current session, returns not a value.
""

function! s:screen.createProject()
    call g:vim_prj#createPrj()
    let self.changes = 1
    call self.redraw()
endfunction

"//

""
" Method showProjectInfo() prints information about project under cursor.
"
" @returns {} - Prints info about project under cursor, returns not a value.
""

function! s:screen.showProjectInfo()
    let l:project = self.getProject()
    call s:System.echo( l:project.name . ' :', 'ModeMsg' )
    let l:timeOfCreation = l:project.timeOfCreation
    let l:timeOfModification = l:project.timeOfModification
    if exists( "*strftime" )
        let l:timeOfCreation =
        \ substitute( l:project.timeOfCreation, '\(\d\d\d\d\)\(\d\d\)\(\d\d\)\(\d\d\)\(\d\d\)\(\d\d\)', '\3\.\2\.\1  \4:\5:\6', '' )
        let l:timeOfModification =
        \ substitute( l:project.timeOfModification, '\(\d\d\d\d\)\(\d\d\)\(\d\d\)\(\d\d\)\(\d\d\)\(\d\d\)', '\3\.\2\.\1  \4:\5:\6', '' )
    endif
    let l:result =
    \[
    \    '    id : ' . l:project.orderId,
    \    '    path to project : ' . l:project.projectPath,
    \    '    current working directory : ' . l:project.pwdPath,
    \    '    time of creation : ' . l:timeOfCreation,
    \    '    time of modification : ' . l:timeOfModification,
    \]
    call s:System.echo( join( l:result, "\n" ) )
endfunction

"//

""
" Method editVimrc() opens '.vimrc' of project under cursor.
" If current working project '.vimrc' file is edited, then the settings can
" be applied after saving.
"
" @returns {} - Opens file '.vimrc' of the project under cursor, returns not a value.
""

function! s:screen.editVimrc()
    let l:project = self.getProject()
    let l:vimrc = s:Path.join( l:project.projectPath, '.vimrc' )
    exe 'silent! tabnew ' . l:vimrc
    if type( g:vim_prj#.currentProject ) == 4
        if l:project.projectPath == g:vim_prj#.currentProject.projectPath
            autocmd BufWritePost <buffer> call s:_updateCurrentConfig()
        endif
    endif
endfunction

function! s:_updateCurrentConfig()
    let l:confirm = s:System.confirm( 'Do you want to apply new settings?' )
    if l:confirm == 1
        exe 'silent! so ' . s:Path.join( g:vim_prj#.currentProject.projectPath, '.vimrc' )
    endif
endfunction

"//

""
" Method renameProject() is intended to rename projects inside projects list buffer.
"
" @returns {} - Returns not a value, renames project.
""

function! s:screen.renameProject()
    call s:System.echo( 'Project name', 'ModeMsg' )
    let l:name = s:System.read( 'Please, enter new name: ' )
    if l:name != ''
        let l:project = self.getProject()
        let l:project.name = l:name
    endif
    let self.changes = 1
    call self.sort( self.projects )
    call self.redraw()
endfunction

"//

""
" Method recordUp() swaps current project position with previous project and
" saves changes in list of projects.
"
" @returns {} - Moves up position of current project, returns not a value.
""

function! s:screen.recordUp()
    if self.sortValue != 'orderId'
        echo 'Unable to change order of projects. Please, use sorting by project ID'
        return
    endif

    let l:prjNum = self.getProjectNumber()
    if l:prjNum - 1 > 0
        let self.projects[ l:prjNum - 1 ].orderId = l:prjNum - 1
        let self.projects[ l:prjNum - 2 ].orderId = l:prjNum
    endif
    let self.changes = 1
    call self.sort( self.projects )
    call self.redraw()
    call cursor( l:prjNum + 1, 1 )
endfunction

"//

""
" Method recordDown() swaps current project position with next project and
" saves changes in list of projects.
"
" @returns {} - Moves down position of current project, returns not a value.
""

function! s:screen.recordDown()
    if self.sortValue != 'orderId'
        echo 'Unable to change order of projects. Please, use sorting by project ID'
        return
    endif

    let l:prjNum = self.getProjectNumber()
    if l:prjNum < len( self.projects )
        let self.projects[ l:prjNum - 1 ].orderId = l:prjNum + 1
        let self.projects[ l:prjNum ].orderId = l:prjNum
    endif
    let self.changes = 1
    call self.sort( self.projects )
    call self.redraw()
    call cursor( l:prjNum + 3, 1 )
endfunction

"//

""
" Method deleteRecord() deletes record in list of projects, method removes not
" directory with project files.
"
" @returns { String } - Deletes record in list of projects, returns a removed
" record.
""

function! s:screen.deleteRecord()
    let l:prjNum = self.getProjectNumber()
    let l:orderId = self.projects[ l:prjNum - 1 ].orderId
    let l:removed = remove( self.projects, l:prjNum - 1 )
    for project in self.projects
        if project.orderId >= l:orderId
            let project.orderId = project.orderId - 1
        endif
    endfor
    let self.changes = 1
    call g:vim_prj#saveProjectList()
    let self.changes = 0
    call self.sort( self.projects )
    call self.redraw()
    call cursor( l:prjNum, 1 )
    return l:removed.projectPath
endfunction

"//

""
" Method deleteProject() deletes record in list of projects and removes all
" project files.
"
" @returns {} - Removes all project data, returns not a value.
""

function! s:screen.deleteProject()
    let l:projectPath = self.deleteRecord()
    call g:vim_prj#deleteProject( l:projectPath )
    echo 'Project deleted from directory: ' . l:projectPath
endfunction

"//

""
" Method showHelp() is intended to show key mapping for buffer with list of
" projects.
"
" @returns {} - Show help for key mapping in current buffer, returns not a value.
""

let s:screen.help =
\[
\    '" Manual "',
\    '',
\    '" Enter - switch to project under cursor',
\    '" Double click - switch to project under cursor',
\    '" aa - add new project',
\    '" <Leader>aa - create project from current session',
\    '" <Leader>dd - delete project under cursor',
\    '" d - delete record in list of projects',
\    '" i - show info about project under cursor',
\    '" e - edit file ".vimrc" of project under cursor',
\    '" cn - rename project',
\    '" C-Up - move up current project record in list',
\    '" C-Down - move down current project record in list',
\    '" S-r - reload list of projects',
\    '" s - sets type of sorting of projects',
\    '" r - reverse order of projects',
\    '',
\    '" <Leader> key for safer operations',
\    ''
\]

function! s:screen.showHelp()
    if s:Content.line( 1 ) != self.help[ 0 ]
        let self.pos = s:Content.pos()
        call s:Content.add( 1, self.help )
    else
        call self.active()
        call s:Content.pos( self.pos )
    endif
endfunction

"//

""
" Method closeWindow() closes window with list of projects.
"
" @returns {} - Close buffer window, returns not a value.
""

function! s:screen.closeWindow()
    call g:vim_prj#showProjectsList()
endfunction

"//

let s:bufStack = s:BufferStack.new()
call s:bufStack.push( s:screen )
call s:screen.ignoreMap( 'n', '<C-y>' )
let vim_prj#Projects# = s:bufStack
