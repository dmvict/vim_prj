" Date Create: 2015-01-17 11:28:44
" Last Change: see git logs
" Author: Artur Sh. Mamedbekov (Artur-Mamedbekov@yandex.ru)
" Contributor: Dmytro K. (dm.vict.kr@gmail.com)
" License: GNU GPL v3 (http://www.gnu.org/copyleft/gpl.html)

let s:Path = vim_lib#base#Path#
let s:File = vim_lib#base#File#
let s:System = vim_lib#sys#System#.new()
let s:Content = vim_lib#sys#Content#.new()

"//

""
" Method showProjectsList() is intended to toggle 'Projects' buffer with a list of
" projects.
" First call of method show window with projects, the second call turn it off.
"
" @returns {} - Toggle window with a list of projects, returns not a value.
""

function! vim_prj#showProjectsList()
    let l:screen = g:vim_prj#Projects#
    if l:screen.current().getWinNum() != -1
        call l:screen.clear( 1 )
        call l:screen.current().unload()
    else
        call l:screen.vactive( 'left', 25 )
        call l:screen.current().option( 'winfixwidth', 1 )
        call s:Content.pos({ 'l' : 3, 'c' : 2 })
    endif
endfunction

"//

""
" Method createPrj() creates project directories and files at specified path.
"
" @param { String } path - Relative or absolute path to directory with project.
" If {-path-} is not provided, method create project directory at current
" working directory.
"
" @returns {} - Creates project directory and files, returns not a value.
""

function! vim_prj#createPrj()
    if g:vim_prj#prjPath != '.'
        let l:word = s:System.read( 'Enter name of project or path: ' )
        if l:word != ''
            let l:dirPrj = s:_createPrj( l:word )
        else
            let l:dirPrj = s:_createPrj()
        endif
    else
        let l:dirPrj = s:_createPrj()
    endif

    call s:System.au( 'VimLeavePre', function( "vim_prj#saveSession" ) )
endfunction

"//

function! s:_createPrj( ... )
    if exists( 'a:1' )
        if s:Path.isAbsolute( a:1 )
            let l:dirPrj = s:File.absolute( a:1 )
        elseif g:vim_prj#prjPath != '.' && fnamemodify( a:1, ':t' ) == a:1
            let l:dirPrj = s:File.absolute( s:Path.join( g:vim_prj#prjPath, a:1 ) )
        else
            let l:dirPrj = s:File.relative( a:1 )
        endif
    else
        let l:dirPrj = s:File.relative( '.vim' )
    endif

    if !l:dirPrj.isExists()
        try
            call l:dirPrj.createDir()
        catch /.*/
            echo ' Cannot create project directory ' . l:dirPrj.getAddress()
            return
        endtry

        if exists( 'a:2' )
            call s:_projectsWrite( l:dirPrj, a:2 )
        else
            call s:_projectsWrite( l:dirPrj )
        endif
    else
        let l:recordAppend = 1
        let l:pwdPath = exists( 'a:2' ) ? a:2 : getcwd()
        for project in g:vim_prj#Projects#.current().projects
            if project.pwdPath == l:pwdPath
                let l:recordAppend = 0
                break
            endif
        endfor
        if l:recordAppend == 1
            if exists( 'a:2' )
                call s:_projectsWrite( l:dirPrj, a:2 )
            else
                call s:_projectsWrite( l:dirPrj )
            endif
        endif
    endif

    let l:pluginsFile = l:dirPrj.getChild( 'plugins.vim' )
    if !l:pluginsFile.isExists()
        call l:pluginsFile.createFile()
    endif

    let l:sessionFile = l:dirPrj.getChild( 'session.vim' )
    if !l:sessionFile.isExists()
        call l:sessionFile.createFile()
        if exists( 'a:2' )
            call l:sessionFile.rewrite
            \([
            \  'let SessionLoad = 1',
            \  'if &cp | set nocp | endif',
            \  'let s:so_save = &so | let s:siso_save = &siso | set so=0 siso=0',
            \  'let v:this_session=expand("<sfile>:p")',
            \  'silent only',
            \  'silent tabonly',
            \  'cd ' . a:2,
            \  'let &so = s:so_save | let &siso = s:siso_save',
            \  'doautoall SessionLoadPost',
            \  'unlet SessionLoad',
            \])
        endif
    endif

    let l:bundleDir = l:dirPrj.getChild( 'bundle' )
    if !l:bundleDir.isExists()
        call l:bundleDir.createDir()
    endif

    let l:vimrc = l:dirPrj.getChild( '.vimrc' )

    if !l:vimrc.isExists()
        call l:vimrc.createFile()
        if g:vim_prj#vimrcPath != '.'
            let l:vimrcPath = s:File.absolute( g:vim_prj#vimrcPath )
            if l:vimrcPath.isExists()
                let l:content = s:File.absolute( g:vim_prj#vimrcPath ).read()
                call l:vimrc.rewrite( l:content )
            endif
        else
            call l:vimrc.rewrite
            \([
            \  'filetype off',
            \  "call vim_lib#sys#Autoload#init('" . l:dirPrjPath . "', 'bundle')",
            \  'so ' . s:Path.join( l:dirPrjPath, 'plugins.vim' ),
            \  'filetype indent plugin on',
            \])
        endif
    endif

    return l:dirPrj
endfunction

"//

function! s:_projectsWrite( dirPrj, ... )
    let l:list = s:File.absolute( g:vim_prj#.prjList )
    if !l:list.getDir().isExists()
        call l:list.getDir().createDir()
    endif
    if !l:list.isExists()
        call l:list.createFile()
    endif

    let l:dirPrjPath = a:dirPrj.getAddress()
    let l:name = fnamemodify( l:dirPrjPath, ':t' )
    if l:name == '.vim'
        let l:name = fnamemodify( l:dirPrjPath, ':h:t' )
    endif

    let l:pwdPath = exists( 'a:1' ) ? a:1 : getcwd()
    let l:timeOfCreation = exists( "*strftime" ) ? strftime( "%Y%m%d%H%M%S" ) : localtime()

    call l:list.write
    \([
    \    l:name . ' :',
    \    '    orderId : ' . ( len( g:vim_prj#Projects#.current().projects ) + 1 ),
    \    '    projectPath : ' . l:dirPrjPath,
    \    '    pwdPath : ' . l:pwdPath,
    \    '    timeOfCreation : ' . l:timeOfCreation,
    \    '    timeOfModification : ' . l:timeOfCreation,
    \])
    call g:vim_prj#Projects#.current().reload()
endfunction

"//

""
" Method addNewPrj() creates empty project directories and files at specified path.
"
" @param { String } path - Relative or absolute path to directory with project.
" If {-path-} is not provided, method echo warning message.
"
" @returns {} - Creates empty project, returns not a value.
""

function! vim_prj#addNewPrj()
    let l:dir = s:System.read( 'Enter path to project directory: ' )
    if l:dir != ''
        if g:vim_prj#prjPath != '.'
            let l:confirm = s:System.confirm( 'Do you want to create local project in ' . l:dir .'?' )
            if l:confirm == 1
                let l:path = s:Path.join( l:dir, '.vim' )
            else
                let l:path = s:Path.join( g:vim_prj#prjPath, fnamemodify( l:dir, ':t' ) )
            endif
        else
            let l:path = s:Path.join( l:dir, '.vim' )
        endif
    else
        echo "\n\nPlease, enter correct path"
    endif

    call s:_createPrj( l:path, l:dir )
endfunction

"//

""
" Method deleteProject() deletes project directory and files at the specified path in first argument {-a:1-}.
" If {-a:1-} is not provided, method deletes current project {-prj-}.
"
" @param { String } a:1 - Path to project directory.
"
" @returns {} - Deletes project directory, returns not a value.
""

function! vim_prj#deleteProject( ... )
    if exists( 'a:1' )
        if s:Path.isAbsolute( a:1 ) && vim_prj#isPrj( a:1 )
            let l:deletedDir = s:File.absolute( a:1 )
        elseif g:vim_prj#prjPath != '.' && g:vim_prj#isPrj( s:Path.join( g:vim_prj#prjPath, a:1 ) )
            let l:deletedDir = s:File.absolute( s:Path.join( g:vim_prj#prjPath, a:1 ) )
        elseif vim_prj#isPrj( s:File.relative( a:1 ).getAddress() )
            let l:deletedDir = s:File.relative( a:1 )
        endif
    elseif type( g:vim_prj#.currentProject ) == 4
        let l:deletedDir = s:File.absolute( g:vim_prj#.currentProject.projectPath )
    else
        echo 'Project directory not exists'
        return
    endif

    call l:deletedDir.deleteDir()
endfunction

"//

""
" Method saveSession() saves current session into the file 'session.vim' at a path provided
" in first argument {-a:1-}.
" If {-a:1-} is not provided, method saves session to file 'session.vim' at the project path {-prj-}.
"
" @param { String } a:1 - Path to file 'session.vim'
"
" @returns {} - Saves session, returns not a value.
""

function! vim_prj#saveSession( ... )
    if !g:vim_prj#savingSession
        return
    endif

    if exists( 'a:1' )
        if s:Path.isAbsolute( a:1 )
            let l:absolutePath = a:1
        elseif g:vim_prj#prjPath != '.' && g:vim_prj#isPrj( s:Path.join( g:vim_prj#prjPath, a:1 ) )
            let l:absolutePath = s:Path.join( g:vim_prj#prjPath, a:1, 'session.vim' )
        else
            let l:absolutePath = s:File.relative( s:Path.join( a:1, 'session.vim' ) ).getAddress()
        endif
    elseif type( g:vim_prj#.currentProject ) == 4
        let l:absolutePath = s:Path.join( g:vim_prj#.currentProject.projectPath, 'session.vim' )
    else
        echo 'Project not exists'
        return
    endif

    if vim_prj#isPrj( s:File.absolute( l:absolutePath ).getDir().getAddress(), fnamemodify( l:absolutePath, ':t' ) )
        set sessionoptions=folds,winsize,help,curdir,localoptions,tabpages
        exe 'mksession! ' . l:absolutePath
        echo 'Saved to ' . l:absolutePath
    endif
endfunction

"//

""
" Method saveLastSession() saves link on last user session. If user uses not a project session, then
"  method saves current session into the file at the special 'session.vim' file.
"
" @returns {} - Saves link on the last session. If uses not project session, then method creates new
" session file, returns not a value.
""

function! vim_prj#saveLastSession()
    let l:lastSessionFile = s:File.absolute( g:vim_prj#.lastSession )
    if !l:lastSessionFile.isExists()
        call l:lastSessionFile.createFile()
    endif

    if type( g:vim_prj#.currentProject ) == 4
        call l:lastSessionFile.rewrite( [ g:vim_prj#.currentProject.projectPath ] )
    else
        let l:sessionDir = l:lastSessionFile.getDir()
        let l:sessionFile = l:sessionDir.getChild( 'session' )
        if !l:sessionFile.isExists()
            call l:sessionFile.createFile()
        endif
        call vim_prj#saveSession( l:sessionFile.getAddress() )
        call l:lastSessionFile.rewrite( [ l:sessionDir.getAddress() ] )

        let l:read = l:sessionFile.read()
        if !s:System.isWindows() " qqq : add replacing paths for windows
            let l:i = 1
            for l:string in l:read
                if matchstr( l:string, '^edit' ) != ''
                    if l:string[ 5 ] != '~' && l:string[ 5 ] != s:Path.slash
                        let l:split = split( l:string )
                        let l:read[ l:i ] = l:split[ 0 ] . ' ' . s:Path.join( getcwd(), l:split[ 1 ] )
                    endif
                endif
                let l:i += 1
            endfor
         endif
        call l:sessionFile.rewrite( l:read )
    endif
endfunction

"//

""
" Method saveProjectList() saves list of projects.
"
" @returns {} - Saves projects into file.
""

function! vim_prj#saveProjectList()
    if g:vim_prj#Projects#.current().changes == 0
        return
    endif

    let l:projectFile = s:File.absolute( g:vim_prj#.prjList )
    if !l:projectFile.isExists()
        return
    endif

    call g:vim_prj#Projects#.current().updateProjectsListByTime()

    let l:result =
    \[
    \    'sortValue : ' . g:vim_prj#Projects#.current().sortValue,
    \    'reverseSort : ' . g:vim_prj#Projects#.current().reverseSort,
    \]
    for project in g:vim_prj#Projects#.current().projects
        let l:modificationTime = project.timeOfModification

        if type( g:vim_prj#.currentProject ) == 4 && project == g:vim_prj#.currentProject
            let l:modificationTime = exists( "*strftime" ) ? strftime( "%Y%m%d%H%M%S" ) : localtime()
        endif

        let l:result +=
        \[
        \    project.name . ' :',
        \    '    orderId : ' . project.orderId,
        \    '    projectPath : ' . project.projectPath,
        \    '    pwdPath : ' . project.pwdPath,
        \    '    timeOfCreation : ' . project.timeOfCreation,
        \    '    timeOfModification : ' . l:modificationTime,
        \]
    endfor

    if len( l:result ) > 2
        call l:projectFile.rewrite( l:result )
    endif
endfunction

"//

""
" Method loadSession() loads session from path provided in first argument {-a:1-}.
" If {-a:1-} is not provided, method loads session fromfile 'session.vim' at the project path {-prj-}.
"
" @param { String } a:1 - Path to file 'session.vim'
"
" @returns {} - Loads session, returns not a value.
""

function! vim_prj#loadSession( ... )
    if !g:vim_prj#loadingSession
        return
    endif

    if exists( 'a:1' )
        if s:Path.isAbsolute( a:1 )
            let l:absolutePath = a:1
        elseif g:vim_prj#prjPath != '.' && g:vim_prj#isPrj( s:Path.join( g:vim_prj#prjPath, a:1 ) )
            let l:absolutePath = s:Path.join( g:vim_prj#prjPath, a:1 )
        else
            let l:absolutePath = s:File.relative( a:1 ).getAddress()
        endif
    elseif type( g:vim_prj#.currentProject ) == 4
        let l:absolutePath = g:vim_prj#.currentProject.projectPath
    else
        echo 'Project not exists'
        return
    endif

    if vim_prj#isPrj( l:absolutePath ) && filereadable( s:Path.join( l:absolutePath, 'session.vim' ) )
        exe 'silent! so '. s:Path.join( l:absolutePath, 'session.vim' )
        if s:File.absolute( s:Path.join( l:absolutePath, '.vimrc' ) ).isExists() && len( s:File.absolute( s:Path.join( l:absolutePath, '.vimrc' ) ).read() ) != 0
            exe 'silent! so ' . s:Path.join( l:absolutePath, '.vimrc' )
        endif
        echo 'Loaded from ' . l:absolutePath
    endif
endfunction

"//

function! vim_prj#loadLastSession()
    if !g:vim_prj#loadLastSession || !s:File.absolute( g:vim_prj#.lastSession ).isExists()
        return
    endif

    if s:File.absolute( g:vim_prj#.lastSession ).read()[ 0 ] == s:File.absolute( g:vim_prj#.lastSession ).getDir().getAddress()
        let l:bufferSession = s:File.absolute( s:Path.join( s:File.absolute( g:vim_prj#.lastSession ).getDir().getAddress(), 'session' ) ).read()
        let l:length = len( l:bufferSession )
        let l:i = 0
        while l:i < l:length
            if matchstr( l:bufferSession[ l:i ], '^\s*cd.*') != ''
                let l:bufferSession[ l:i ] = 'cd ' . getcwd()
                call s:File.absolute( s:Path.join( s:File.absolute( g:vim_prj#.lastSession ).getDir().getAddress(), 'session' ) ).rewrite( l:bufferSession )
                break
            endif
            let l:i += 1
        endwhile

        exe 'silent! so '. s:Path.join( s:File.absolute( g:vim_prj#.lastSession ).getDir().getAddress(), 'session' )
        echo 'Loaded from ' . s:Path.join( s:File.absolute( g:vim_prj#.lastSession ).getDir().getAddress(), 'session' )
    else
        let l:path = s:File.absolute( g:vim_prj#.lastSession ).read()[ 0 ]
        let l:name = fnamemodify( l:path, ':t' )
        if l:name  == '.vim'
            let l:name = fnamemodify( l:path, ':h:t' )
        endif
        let l:confirm = s:System.confirm( 'Do you want to open project: ' . l:name . '?' )
        if l:confirm == 1
            let g:vim_prj#.currentProject = 0
            for project in g:vim_prj#Projects#.current().projects
                if project.projectPath == l:path
                    let g:vim_prj#.currentProject = project
                endif
            endfor
            exe 'silent source ' . s:Path.join( g:vim_prj#.currentProject.projectPath, 'session.vim' )
            call s:System.au( 'VimLeavePre', function( "vim_prj#saveSession" ) )
            if s:File.absolute( s:Path.join( g:vim_prj#.currentProject.projectPath, '.vimrc' ) ).isExists() && len( s:File.absolute( s:Path.join( g:vim_prj#.currentProject.projectPath, '.vimrc' ) ).read() ) != 0
                exe 'silent! so ' . s:Path.join( g:vim_prj#.currentProject.projectPath, '.vimrc' )
            endif
        endif
    endif
endfunction


""
" Method checks wheter the path is a project directory.
"
" @param { String } a:1 - Path to the project directory. If path is not" provided, then method checks
" current working directory.
" @param { String } a:2 - Name of file with session data. If name is not provided, then method checks
" file `session.vim`
"
" @return { Boolean } - Return true if the specified path exists.
""

function! vim_prj#isPrj( ... )
    if exists( 'a:1' )
        let l:path = s:File.absolute( a:1 )
    else
        let l:path = s:File.absolute( s:Path.join( getcwd(), '.vim' ) )
    endif

    if exists( 'a:2' )
        let l:name = a:2
    else
        let l:name = 'session.vim'
    endif

    return l:path.isExists() && l:path.getChild( l:name ).isExists() && l:path.getChild( l:name ).isFile()
endfunction
