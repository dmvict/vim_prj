set syntax=vim_lib-tmp

syntax sync fromstart
syntax match BufferName /\" Projects \"/
syntax match NumberPoint /\<[0-9]\+\ze\]/
syntax match ProjectName /\zs\s\{2}[a-zA-z0-9\.\-]*\(]\)\@!\ze\n/
syntax match CurrentProject /\zs\s\{2}[a-zA-z0-9\.\-]*\(]\)\@! \*\ze\n/

highlight default link BufferName     Identifier
highlight default link NumberPoint    Number
highlight default link ProjectName    String
highlight default link CurrentProject Type
