" Date Create: 2015-01-17 10:48:16
" Last Change: see git logs
" Author: Artur Sh. Mamedbekov (Artur-Mamedbekov@yandex.ru)
" Contributor: Dmytro K. (dm.vict.kr@gmail.com)
" License: GNU GPL v3 (http://www.gnu.org/copyleft/gpl.html)

let s:Path = vim_lib#base#Path#
let s:File = vim_lib#base#File#
let s:Plugin = vim_lib#sys#Plugin#
let s:System = vim_lib#sys#System#.new()

"//

let s:p = s:Plugin.new( 'vim_prj', '1' )

""
" @param { Boolean } savingSession - Enable saving of last user session.
""

if( !exists( 'g:vim_prj#savingSession' ) )
    let g:vim_prj#savingSession = 1
endif

""
" @param { Boolean } loadingSession - Enable autoloading of saved session.
""

if( !exists( 'g:vim_prj#loadingSession' ) )
    let g:vim_prj#loadingSession = 1
endif

""
" @param { Boolean } loadSession - Enable autoloading of last user session.
""

if( !exists( 'g:vim_prj#loadLastSession' ) )
    let g:vim_prj#loadLastSession = 1
endif

""
" @param { String } prjPath - Path to directory with projects. By default it has value '.', and creates
" local project directory '.vim' in opened directory.
""

if( !exists( 'g:vim_prj#prjPath' ) )
    let g:vim_prj#prjPath = '.'
endif

""
" @param { String } vimrcPath - Path to configuration for '.vimrc' file. Content of this file will copy
" to local project '.vimrc' file.
""

if( !exists( 'g:vim_prj#vimrcPath' ) )
    let g:vim_prj#vimrcPath = '.'
endif

""
" @param { String } .prjList - Path to directory with 'ProjectsList' file. This file contents list of projects.
""

let s:p.prjList = s:Path.join( expand( '<sfile>:p:h' ), 'vim_prj', 'ProjectsList' )

""
" @param { String } .lastSession - Path to directory with 'LastSession' file. This file contents data for
" loading last user session.
""

let s:p.lastSession = s:Path.join( expand( '<sfile>:p:h' ), 'vim_prj', 'LastSession' )

""
" @var { Map } opt - Project options.
""

if !exists( 'g:vim_prj#opt' )
    let g:vim_prj#opt = {}
endif

"//

""
" Method run() provides initializing of project. It loads project and adds event
" handler for its saving.
""

function! s:p.run()
    let self.global = s:Path.join( $VIM, 'vimfiles' )
    if s:System.isWindows()
        let self.user = s:Path.join( $HOME, 'vimfiles' )
    else
        let self.user = s:Path.join( $HOME, '.vim' )
    endif

    let self.currentProject = 0
    let l:pwd = getcwd()
    for project in g:vim_prj#Projects#.current().projects
        if project.pwdPath == l:pwd
            let self.currentProject = project
        endif
    endfor

    call s:System.au( 'VimLeavePre', function( "vim_prj#saveLastSession" ) )
    call s:System.au( 'VimLeavePre', function( "vim_prj#saveProjectList" ) )
    if type( self.currentProject ) == 4 && len( argv() ) == 0
        call s:System.au( 'VimLeavePre', function( "vim_prj#saveSession" ) )
        call s:System.au( 'VimEnter', function( "vim_prj#loadSession" ) )
    elseif len( argv() ) == 0
        call s:System.au( 'VimEnter', function( "vim_prj#loadLastSession" ) )
    endif

    set exrc
    set nosecure
endfunction

"//

""
" Registering of the class
""

call s:p.reg()
